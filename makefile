SOURCES = main.c mem.h mem.c
OBJS = ${SOURCES:.c=.o}
EXEC = lab6

GCC = gcc
GCC_FLAGS = -o

all:$(EXEC)

$(EXEC) : $(OBJS)
	$(GCC) $(OBJS)  -o $@

%.o : %.c
	$(GCC) $< -c -o $@

main.c: mem.h
clean :
	rm -rf $(OBJS)
